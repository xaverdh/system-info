module Table where

import SystemInfo

table :: [InfoSpec]
table =
  [ kernel
  , uptime
  , date
  , audio
  , light
  , network
  , battery
  , temp
  , ram ]

