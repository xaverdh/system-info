{-# LANGUAGE OverloadedStrings, LambdaCase, ScopedTypeVariables, TupleSections #-}
module SystemInfo where

import System.Process
import System.Directory (listDirectory)
import System.FilePath ((</>))
import System.Exit
import qualified System.IO.Error as IOE
import Text.Read
import Text.Printf
import Text.Regex.Applicative
import Text.Regex.Applicative.Common

import Control.Monad
import Control.Applicative
import Control.Concurrent
import Data.List
import Data.Bool
import Data.Maybe
import Data.Functor
import qualified Text.PrettyPrint.ANSI.Leijen as PP

data Color = 
  White
  | Yellow
  | Magenta
  | Cyan
  | Green
  | Red
  | Blue
  | None

colorX :: Color -> Maybe String
colorX = \case
  None -> Nothing
  c -> Just $ case c of 
    White -> "white"
    Yellow -> "yellow"
    Cyan -> "cyan"
    Magenta -> "magenta"
    Green -> "#00F000"
    Red -> "red"
    Blue -> "blue"

colorT :: Color -> PP.Doc -> PP.Doc
colorT = \case
  White -> PP.white
  Yellow -> PP.yellow
  Cyan -> PP.cyan
  Magenta -> PP.magenta
  Green -> PP.green
  Red -> PP.dullred
  Blue -> PP.blue
  None -> id


data InfoSpec = InfoSpec
  { specName :: String
  , specColor :: Color
  , specAction :: IO [String] }

call :: FilePath -> [String] -> IO String
call name args = flip IOE.catchIOError onFail
  $ readProcess name args ""
  where
   onFail e = pure $ "IO Error: " <> show e


kernel = InfoSpec "Kernel" White
 ( pure <$> call "uname" ["-r"] )

uptime = InfoSpec "Uptime" White
  ( pure . refmtUptime . takeWhile (/=' ') <$> readFile "/proc/uptime" )
  where
    refmtUptime = maybe "<Format Error>" compUptime . readMaybe

    compUptime :: Double -> String
    compUptime v = 
      let (m',s) = round v `divMod` 60 
          (h',m) = m' `divMod` 60 
          (d,h) = h' `divMod` 24
       in intercalate ":"
         [show d ++ "d",show h ++ "h",show m ++ "m",show s ++ "s"]
 
date = InfoSpec "Date" Magenta
  ( pure <$> call "date" ["+%H:%M on %a %d %b %Y"] )

audio = InfoSpec "Audio" Cyan
  ( fmtAudio <$> call "ponymix" ["get-volume"] <*> getMuted )
  where
    fmtAudio :: String -> Bool -> [String]
    fmtAudio vol muted = 
      pure $ printf "%s%%, %s" vol (bool "on" "muted" muted :: String)
      
    getMuted = do
      (b,_,_) <- readProcessWithExitCode "ponymix" ["is-muted" ] ""
      pure $ case b of
        ExitSuccess -> True
        ExitFailure _ -> False

light = InfoSpec "Light" Cyan
  ( pure <$> call "light" ["-G"] <> (pure "%") )

network = InfoSpec "Network" Yellow
  ( fmtNetworks <$> call "networkctl" ["--no-pager","--no-legend","list"] )
  where
    fmtNetworks raw = join . map fmtLine $ lines raw

    fmtLine l = case words l of
      num : iface : kind : opstatus : setup : _ ->
        [ printf "(%s) %s, %s" iface opstatus setup ]
      _ -> []

battery = InfoSpec "Battery" Green
  ( pure . refmtBat <$> call "acpi" ["-b"] )
  where 
    refmtBat = unwords . drop 1 . words . dropWhile (/= ':')

temp = InfoSpec "Temp" Red $ do
  subs <- lshwmon
  tables <- mapM hwmonTable subs
  pure $ uncurry (printf "(%s) %s°C") <$> join tables
  where
    hwmonPath = "/sys/class/hwmon"
    lshwmon = listDirectory hwmonPath

    hwmonTable p = do
      name <- readFile (hwmonPath </> p </> "name")
      if not (isTemp name)
        then pure []
        else do
          paths <- getTempPaths p
          fmap (name,) <$> mapM readTemp paths

    getTempPaths p = fmap (p </>) . filter isTempInput
      <$> listDirectory (hwmonPath </> p)
    isTempInput = isJust . (=~ "temp" *> void digit <* "_input")

    readTemp p = do
      raw <- readFile (hwmonPath </> p)
      pure $ case readMaybe raw of
        Just (i::Integer) -> show $ i `div` 1000
        Nothing -> "<Invalid value>"

    isAcpiTZ = isJust . (=~ "acpitz" <* optional "\n")
    isCoretemp = isJust . (=~ "coretemp" <* optional "\n")
    isTemp = isAcpiTZ -|- isCoretemp

    (-|-) = liftA2 (||)


ram = InfoSpec "Memory" Yellow
  ( refmtMem <$> call "free" ["-m","-h"] )
  where
    refmtMem raw = case lines raw of
      _ : ram : swap : _ -> join [ refmtLine ram, refmtLine swap ]
      _ -> []

    refmtLine l = case words l of
      kind:total:used:_ ->
        pure $ printf "%s %s / %s" (refmtKind kind) used total
      _ -> []

    refmtKind s
      | s  == "Mem:" = "(ram)"
      | s == "Swap:" = "(swap)"
      | otherwise = s

withData :: [InfoSpec] -> (String -> Color -> String -> a) -> IO [a]
withData table f = do
  mvars <- forM table $ \(InfoSpec name color action) -> do
    mvar <- newEmptyMVar
    forkIO $ action >>= putMVar mvar . map (f name color)
    pure mvar
  join <$> forM mvars takeMVar

cleanup :: String -> String
cleanup = filter (/='\n')

xMain :: [InfoSpec] -> IO ()
xMain table = do
  info <- withData table fmtData
  callProcess "/usr/bin/env"
    [ "notify-send", "-t", "20000", "System Info", unlines info ]
  where
    fmtData name color value =
      name <> ": " <> fmtValue (cleanup value) color

    fmtValue value color = case colorX color of
      Nothing -> value
      Just xcolor ->
        printf "<span foreground=\"%s\">%s</span>" xcolor value

ttyMain :: [InfoSpec] -> IO ()
ttyMain table = do
  info <- withData table $ \name color value ->
    PP.string name <> ": "
    <> colorT color ( PP.string $ cleanup value )
  PP.putDoc $ PP.vsep info <> PP.hardline


